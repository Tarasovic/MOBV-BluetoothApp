package sk.stuba.fei.mobv.feibluetoothscanner.locator;

import java.util.List;

import sk.stuba.fei.mobv.feibluetoothscanner.model.BluetoothAP;
import sk.stuba.fei.mobv.feibluetoothscanner.model.Position;

interface BluetoothLocatorListener {
    void onListAccessPointsForPositionUpdate(List<BluetoothAP> points);

    void onListAccessPointsForPositionFinish(List<BluetoothAP> points);

    void onDeviceLocationUpdate(Position likelyPosition);

    void onDeviceLocationFinish(Position position);
}
