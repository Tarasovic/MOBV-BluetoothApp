package sk.stuba.fei.mobv.feibluetoothscanner.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;

import sk.stuba.fei.mobv.feibluetoothscanner.R;
import sk.stuba.fei.mobv.feibluetoothscanner.model.BluetoothAP;

public class AccessPointsViewAdapter extends RecyclerView.Adapter<AccessPointsViewAdapter.ItemViewHolder> {
    private final UpdateACPointsCallback updateACPointsCallback;
    private final List<BluetoothAP> bluetoothAPs;
    private final boolean itemsEnabled;

    public AccessPointsViewAdapter(List<BluetoothAP> bluetoothAPs, UpdateACPointsCallback acPointsCallback, boolean itemsEnabled) {
        this.bluetoothAPs = bluetoothAPs;
        this.updateACPointsCallback = acPointsCallback;
        this.itemsEnabled = itemsEnabled;
    }

    @Override
    public AccessPointsViewAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.access_point_item, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AccessPointsViewAdapter.ItemViewHolder holder, int position) {
        holder.acPointNameTextView.setText(bluetoothAPs.get(position).getName());
        holder.acPointBssidTextView.setText(bluetoothAPs.get(position).getBssid());
        holder.acPointIsActiveCheckBox.setChecked(bluetoothAPs.get(position).isStored());

        if (bluetoothAPs.get(position).isMainPoint()) {
            holder.acPointNameTextView.setText(holder.acPointNameTextView.getText() + " - MAIN");
        }

        final int itemsPosition = position;
        holder.acPointIsActiveCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    updateACPointsCallback.addACPoint(bluetoothAPs.get(itemsPosition));
                } else {
                    updateACPointsCallback.removeACPoint(bluetoothAPs.get(itemsPosition));
                }
            }
        });

        holder.view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                holder.acPointIsActiveCheckBox.setChecked(true);
                updateACPointsCallback.addACPoint(bluetoothAPs.get(itemsPosition));
                updateACPointsCallback.setMainACPoint(bluetoothAPs.get(itemsPosition));
                return true;
            }
        });

        holder.acPointIsActiveCheckBox.setEnabled(itemsEnabled);
        holder.view.setEnabled(itemsEnabled);
    }

    @Override
    public int getItemCount() {
        return bluetoothAPs.size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        final View view;
        final TextView acPointNameTextView;
        final TextView acPointBssidTextView;
        final CheckBox acPointIsActiveCheckBox;

        ItemViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;
            this.acPointNameTextView = (TextView) itemView.findViewById(R.id.ac_point_name);
            this.acPointBssidTextView = (TextView) itemView.findViewById(R.id.ac_point_bssid);
            this.acPointIsActiveCheckBox = (CheckBox) itemView.findViewById(R.id.ac_point_checkbox_active);
        }
    }
}
