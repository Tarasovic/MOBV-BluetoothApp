package sk.stuba.fei.mobv.feibluetoothscanner;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.app.SearchManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import sk.stuba.fei.mobv.feibluetoothscanner.adapters.PositionsDashboardViewAdapter;
import sk.stuba.fei.mobv.feibluetoothscanner.data.FirebaseWrapper;
import sk.stuba.fei.mobv.feibluetoothscanner.data.FirebaseWrapperListener;
import sk.stuba.fei.mobv.feibluetoothscanner.dialogs.AboutDialogFragment;
import sk.stuba.fei.mobv.feibluetoothscanner.dialogs.EnableConnectionDialogFragment;
import sk.stuba.fei.mobv.feibluetoothscanner.dialogs.PositionItemDeleteDialogFragment;
import sk.stuba.fei.mobv.feibluetoothscanner.dividers.DividerItemDecoration;
import sk.stuba.fei.mobv.feibluetoothscanner.locator.BluetoothLocator;
import sk.stuba.fei.mobv.feibluetoothscanner.locator.BluetoothLocatorListenerAdapter;
import sk.stuba.fei.mobv.feibluetoothscanner.model.Position;
import sk.stuba.fei.mobv.feibluetoothscanner.utils.Utils;

public class DashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        FirebaseWrapperListener, PositionItemDeleteDialogFragment.PositionItemDeleteListener {
    public static final String SERIALIZABLE_KEY_POSITION = "fei_position_serializable_key";
    private static final String LISTENER_TAG = "DashboardActivity";
    private static final String LOG_TAG = "DashboardActivity";
    private static final int ENABLE_BLUETOOTH_REQUEST = 666;

    private RecyclerView dashboardRecyclerView;
    private List<Position> positions;
    private BluetoothLocator locator;
    private Toast toast;

    private ImageView positionIcon;
    private ProgressBar progressBar;

    @SuppressLint("ShowToast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //turn pn bluetooth on startup
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, ENABLE_BLUETOOTH_REQUEST);
        }

        //check if there is internet connection on startup
        if (!isNetworkAvailable()) {
            DialogFragment enableConnectionDialogFragment = new EnableConnectionDialogFragment();
            enableConnectionDialogFragment.show(getFragmentManager(), "ConnectionDialog");
        }

        toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);

        setContentView(R.layout.navigation_drawer_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AddPositionActivity.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        positionIcon = (ImageView) findViewById(R.id.positionIcon);

        FirebaseWrapper wrapper = FirebaseWrapper.getInstance();
        wrapper.addListener(LISTENER_TAG, this);
        positions = wrapper.getPositions();

        dashboardRecyclerView = (RecyclerView) findViewById(R.id.dashboardPositionsRecyclerView);
        dashboardRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        dashboardRecyclerView.setItemAnimator(new DefaultItemAnimator());
        dashboardRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext()));
        dashboardRecyclerView.setAdapter(new PositionsDashboardViewAdapter(positions, getApplicationContext()));
        initializeSwipeForRecyclerView();

        locator = BluetoothLocator.getInstance();
        final TextView positionDescTextView = (TextView) findViewById(R.id.positionDesc);
        final TextView positionStatusTextView = (TextView) findViewById(R.id.positionStatus);

        locator.addLocatorListener(LISTENER_TAG, new BluetoothLocatorListenerAdapter() {
            @Override
            public void onDeviceLocationUpdate(Position likelyPosition) {
                positionDescTextView.setText(Utils.getPositionAbbreviation(likelyPosition) + " - " + likelyPosition.getTitle());
            }

            @Override
            public void onDeviceLocationFinish(Position position) {
                if (position == null) {
                    positionDescTextView.setText(getString(R.string.positionNotFound));
                    positionStatusTextView.setText(getString(R.string.actualPositionStatusNotFound));
                } else {
                    positionDescTextView.setText(Utils.getPositionAbbreviation(position) + " - " + position.getTitle());
                    positionStatusTextView.setText(getString(R.string.actualPositionStatusFound));
                }
                positionIcon.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                Utils.showToastMessage(toast, "Device localization ended!", Toast.LENGTH_SHORT);
            }
        });

        LinearLayout currentPosition = (LinearLayout) findViewById(R.id.currentPosition);
        currentPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locator.locateDevice(getApplicationContext());
                positionIcon.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                Utils.showToastMessage(toast, "Locating device!", Toast.LENGTH_SHORT);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        locator.locateDevice(getApplicationContext());
        positionIcon = (ImageView) findViewById(R.id.positionIcon);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        positionIcon.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        Utils.showToastMessage(toast, "Locating device!", Toast.LENGTH_SHORT);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_activity_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String newText) {
                return handleSearchExecution(newText);
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return handleSearchExecution(newText);
            }
        });

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ENABLE_BLUETOOTH_REQUEST) {
            if (resultCode == RESULT_CANCELED) {
                finishAffinity();
            }
        }
    }

    private boolean handleSearchExecution(String text) {
        if (positions == null) {
            return false;
        }

        if (text == null || text.isEmpty()) {
            dashboardRecyclerView.setAdapter(new PositionsDashboardViewAdapter(positions, getApplicationContext()));

        } else {
            List<Position> matchedPositions = new ArrayList<>();
            for (Position position : positions) {
                if (position.getBuilding().toLowerCase().contains(text.toLowerCase())
                        || position.getTitle().toLowerCase().contains(text.toLowerCase())) {
                    matchedPositions.add(position);
                }
            }

            dashboardRecyclerView.setAdapter(new PositionsDashboardViewAdapter(matchedPositions, getApplicationContext()));
        }

        return true;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void initializeSwipeForRecyclerView() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                if (direction == ItemTouchHelper.LEFT) {
                    if (dashboardRecyclerView.getAdapter() instanceof PositionsDashboardViewAdapter) {
                        int itemPosition = viewHolder.getAdapterPosition();
                        Position position = ((PositionsDashboardViewAdapter) dashboardRecyclerView.getAdapter()).getItemOnPosition(itemPosition);

                        PositionItemDeleteDialogFragment dialog = PositionItemDeleteDialogFragment.newInstance(position, itemPosition);
                        dialog.show(getSupportFragmentManager(), "positionItemDeleteDialog");
                    } else {
                        Log.e(LOG_TAG, "Dashboard recycler view has wrong adapter.");
                    }
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                Paint p = new Paint();
                Bitmap icon;

                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE && Math.abs(dX) > 20) {
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    p.setColor(Color.parseColor("#D32F2F"));
                    RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),
                            (float) itemView.getRight(), (float) itemView.getBottom());
                    c.drawRect(background, p);

                    icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_forever_black_24dp);
                    RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width,
                            (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                    c.drawBitmap(icon, null, icon_dest, p);
                }

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                super.clearView(recyclerView, viewHolder);
                getDefaultUIUtil().clearView(((PositionsDashboardViewAdapter.DashboardItemViewHolder) viewHolder).getView());
            }
        };

        if (dashboardRecyclerView != null) {
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
            itemTouchHelper.attachToRecyclerView(dashboardRecyclerView);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_draw_navigation) {
            Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_draw_about) {
            AboutDialogFragment aboutDialogFragment = new AboutDialogFragment();
            aboutDialogFragment.show(getSupportFragmentManager(), "AboutDialogFragment");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onDataChange(List<Position> positions) {
        this.positions = positions;
        dashboardRecyclerView.setAdapter(new PositionsDashboardViewAdapter(this.positions, getApplicationContext()));
    }

    @Override
    public void onInitialDataLoaded(List<Position> positions) {
    }

    @Override
    public void onPositionItemDeletePositiveClick(String positionId, int positionIndex) {
        FirebaseWrapper.getInstance().removePosition(positionId);
        if (dashboardRecyclerView.getAdapter() instanceof PositionsDashboardViewAdapter) {
            ((PositionsDashboardViewAdapter) dashboardRecyclerView.getAdapter()).refreshAdapterForDeletedItem(positionIndex);
        }
    }

    @Override
    public void onPositionItemDeleteNegativeClick() {
        if (dashboardRecyclerView.getAdapter() instanceof PositionsDashboardViewAdapter) {
            ((PositionsDashboardViewAdapter) dashboardRecyclerView.getAdapter()).refreshAdapter();
        }
    }
}
