package sk.stuba.fei.mobv.feibluetoothscanner.utils;

import android.annotation.SuppressLint;
import android.widget.Toast;

import java.text.SimpleDateFormat;

import sk.stuba.fei.mobv.feibluetoothscanner.model.Position;

@SuppressLint("SimpleDateFormat")
public class Utils {
    public static SimpleDateFormat DATE_FORMAT;

    static {
        DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
    }

    public static void showToastMessage(Toast toast, String message, int duration) {
        toast.setText(message);
        toast.setDuration(duration);
        toast.show();
    }

    public static String getPositionAbbreviation(Position position) {
        return position.getBuilding().substring(0, 1) + position.getFloor();
    }
}
