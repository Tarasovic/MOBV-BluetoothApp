package sk.stuba.fei.mobv.feibluetoothscanner.adapters;

import sk.stuba.fei.mobv.feibluetoothscanner.model.BluetoothAP;

public interface UpdateACPointsCallback {
    void addACPoint(BluetoothAP ap);

    void removeACPoint(BluetoothAP ap);

    void setMainACPoint(BluetoothAP ap);
}
