package sk.stuba.fei.mobv.feibluetoothscanner.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import sk.stuba.fei.mobv.feibluetoothscanner.R;
import sk.stuba.fei.mobv.feibluetoothscanner.model.DirectionPoint;

public class DirectionPointsViewAdapter extends BaseAdapter {
    private final ArrayList<DirectionPoint> directionsPointsList;
    private final Context context;

    public DirectionPointsViewAdapter(Context context, ArrayList<DirectionPoint> directionsPointsList) {
        this.context = context;
        this.directionsPointsList = directionsPointsList;
    }

    @Override
    public int getCount() {
        return this.directionsPointsList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.directionsPointsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    @NonNull
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            view = layoutInflater.inflate(R.layout.direction_item, null);
        }

        DirectionPoint point = (DirectionPoint) getItem(position);
        if (point != null) {
            TextView directionPointNumber = (TextView) view.findViewById(R.id.directionPointNumber);
            TextView directionBlockName = (TextView) view.findViewById(R.id.directionBlockName);
            TextView directionsInfoText = (TextView) view.findViewById(R.id.directionDefaultTextDescription);

            directionPointNumber.setText("" + (position + 1) + ".");
            directionBlockName.setText(point.getBuildingDescription());
            directionsInfoText.setText(point.getTextDescription());
        }
        return view;
    }
}
