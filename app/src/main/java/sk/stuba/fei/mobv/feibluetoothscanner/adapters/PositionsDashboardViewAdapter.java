package sk.stuba.fei.mobv.feibluetoothscanner.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sk.stuba.fei.mobv.feibluetoothscanner.DashboardActivity;
import sk.stuba.fei.mobv.feibluetoothscanner.PositionAccessPointsActivity;
import sk.stuba.fei.mobv.feibluetoothscanner.R;
import sk.stuba.fei.mobv.feibluetoothscanner.model.Position;
import sk.stuba.fei.mobv.feibluetoothscanner.utils.Utils;

public class PositionsDashboardViewAdapter extends RecyclerView.Adapter<PositionsDashboardViewAdapter.DashboardItemViewHolder> {
    private static final String LOG_TAG = "DashboardViewAdapter";

    private final Context context;
    private final List<Position> positions;

    public PositionsDashboardViewAdapter(List<Position> positions, Context context) {
        this.context = context;
        this.positions = positions;
    }

    @Override
    public DashboardItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.position_item, parent, false);
        return new DashboardItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DashboardItemViewHolder holder, int position) {
        holder.posAbbreviationTextView.setText(Utils.getPositionAbbreviation(positions.get(position)));
        holder.posNameTextView.setText(positions.get(position).getTitle());
        holder.posDateTextView.setText(positions.get(position).getScanDate());

        final int detailItemPosition = position;
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PositionAccessPointsActivity.class);
                intent.putExtra(DashboardActivity.SERIALIZABLE_KEY_POSITION, positions.get(detailItemPosition));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return positions.size();
    }

    public Position getItemOnPosition(int itemPosition) {
        try {
            return positions.get(itemPosition);
        } catch (IndexOutOfBoundsException e) {
            Log.e(LOG_TAG, "Can not retrieve item on position " + itemPosition + ". ", e);
        }

        return null;
    }

    public void refreshAdapter() {
        notifyDataSetChanged();
    }

    public void refreshAdapterForDeletedItem(int itemPosition) {
        notifyItemRemoved(itemPosition);
    }

    public static class DashboardItemViewHolder extends RecyclerView.ViewHolder {
        final View view;
        final TextView posAbbreviationTextView;
        final TextView posNameTextView;
        final TextView posDateTextView;

        DashboardItemViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;
            this.posAbbreviationTextView = (TextView) itemView.findViewById(R.id.positionAbbreviation);
            this.posNameTextView = (TextView) itemView.findViewById(R.id.positionName);
            this.posDateTextView = (TextView) itemView.findViewById(R.id.positionUpdateDate);
        }

        public View getView() {
            return view;
        }
    }
}


