package sk.stuba.fei.mobv.feibluetoothscanner.locator;

import java.util.List;

import sk.stuba.fei.mobv.feibluetoothscanner.model.BluetoothAP;
import sk.stuba.fei.mobv.feibluetoothscanner.model.Position;

public class BluetoothLocatorListenerAdapter implements BluetoothLocatorListener {
    @Override
    public void onListAccessPointsForPositionUpdate(List<BluetoothAP> points) {
        //do nothing for default implementation
    }

    @Override
    public void onListAccessPointsForPositionFinish(List<BluetoothAP> points) {
        //do nothing for default implementation
    }

    @Override
    public void onDeviceLocationUpdate(Position likelyPosition) {
        //do nothing for default implementation
    }

    @Override
    public void onDeviceLocationFinish(Position position) {
        //do nothing for default implementation
    }
}
