package sk.stuba.fei.mobv.feibluetoothscanner.model;

import android.support.annotation.NonNull;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.List;

@IgnoreExtraProperties
public class Position implements Serializable, Comparable<Position> {
    private String id;
    private String title;
    private int floor;
    private String building;
    private String scanDate;
    private List<BluetoothAP> bluetoothPoints;
    @Exclude
    private boolean stored = true;

    public Position() {
    }

    public Position(String id, String title, int floor, String building, String scanDate,
                    List<BluetoothAP> bluetoothPoints) {
        this.id = id;
        this.title = title;
        this.floor = floor;
        this.building = building;
        this.scanDate = scanDate;
        this.bluetoothPoints = bluetoothPoints;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getScanDate() {
        return scanDate;
    }

    public void setScanDate(String scanDate) {
        this.scanDate = scanDate;
    }

    public List<BluetoothAP> getBluetoothPoints() {
        return bluetoothPoints;
    }

    public void setBluetoothPoints(List<BluetoothAP> bluetoothPoints) {
        this.bluetoothPoints = bluetoothPoints;
    }

    @Exclude
    public boolean isStored() {
        return stored;
    }

    @Exclude
    public void setStored(boolean stored) {
        this.stored = stored;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        return id.equals(position.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "Position{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", floor=" + floor +
                ", building='" + building + '\'' +
                ", scanDate='" + scanDate + '\'' +
                ", bluetoothPoints=" + bluetoothPoints +
                '}';
    }

    @Override
    public int compareTo(@NonNull Position other) {
        return this.getTitle().toLowerCase().compareTo(other.getTitle().toLowerCase());
    }
}
