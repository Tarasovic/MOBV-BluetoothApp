package sk.stuba.fei.mobv.feibluetoothscanner.data;

import java.util.List;

import sk.stuba.fei.mobv.feibluetoothscanner.model.Position;

public interface FirebaseWrapperListener {
    void onDataChange(List<Position> positions);

    void onInitialDataLoaded(List<Position> positions);
}
