package sk.stuba.fei.mobv.feibluetoothscanner.adapters;

import sk.stuba.fei.mobv.feibluetoothscanner.model.Position;

public interface NavigationPositionsCallback {
    void handlePositionItemClick(Position position);
}
