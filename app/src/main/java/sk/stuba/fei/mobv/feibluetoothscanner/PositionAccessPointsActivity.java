package sk.stuba.fei.mobv.feibluetoothscanner;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import sk.stuba.fei.mobv.feibluetoothscanner.adapters.AccessPointsViewAdapter;
import sk.stuba.fei.mobv.feibluetoothscanner.adapters.UpdateACPointsCallback;
import sk.stuba.fei.mobv.feibluetoothscanner.data.FirebaseWrapper;
import sk.stuba.fei.mobv.feibluetoothscanner.locator.BluetoothLocator;
import sk.stuba.fei.mobv.feibluetoothscanner.locator.BluetoothLocatorListenerAdapter;
import sk.stuba.fei.mobv.feibluetoothscanner.model.BluetoothAP;
import sk.stuba.fei.mobv.feibluetoothscanner.model.Position;
import sk.stuba.fei.mobv.feibluetoothscanner.utils.Utils;

public class PositionAccessPointsActivity extends AppCompatActivity implements UpdateACPointsCallback {
    private static final String LISTENER_TAG = "PositionAccessPointsActivity";

    private Position currentPosition;
    private Toast toast;
    private Button saveButton;
    private List<BluetoothAP> bluetoothAPs;
    private RecyclerView recyclerView;
    private ProgressBar progressScanningBTPoints;

    @SuppressLint("ShowToast")
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_position_access_points);

        toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);
        progressScanningBTPoints = (ProgressBar) findViewById(R.id.progressScanningBTPoints);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        currentPosition = (Position) getIntent().getSerializableExtra(DashboardActivity.SERIALIZABLE_KEY_POSITION);
        if (currentPosition == null) {
            Utils.showToastMessage(toast, "No data!", Toast.LENGTH_SHORT);
            return;
        }

        setTitle(currentPosition.getTitle());

        // recycler view setup
        recyclerView = (RecyclerView) findViewById(R.id.access_points_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        bluetoothAPs = currentPosition.getBluetoothPoints();
        if (bluetoothAPs == null) {
            Utils.showToastMessage(toast, "No AC points! Scan started!", Toast.LENGTH_SHORT);
            bluetoothAPs = new ArrayList<>();
        } else {
            Utils.showToastMessage(toast, "Scan started!", Toast.LENGTH_SHORT);
        }
        recyclerView.setAdapter(new AccessPointsViewAdapter(new ArrayList<>(bluetoothAPs), this, false));
        progressScanningBTPoints.setVisibility(View.VISIBLE);

        final BluetoothLocator locator = BluetoothLocator.getInstance();

        locator.addLocatorListener(LISTENER_TAG, new BluetoothLocatorListenerAdapter() {
            @Override
            public void onListAccessPointsForPositionUpdate(List<BluetoothAP> points) {
                bluetoothAPs = points;
                recyclerView.setAdapter(new AccessPointsViewAdapter(points, PositionAccessPointsActivity.this, false));
            }

            @Override
            public void onListAccessPointsForPositionFinish(List<BluetoothAP> points) {
                bluetoothAPs = points;
                onUpdatingPointsEnded();
                Utils.showToastMessage(toast, "Scan finished!", Toast.LENGTH_SHORT);
                progressScanningBTPoints.setVisibility(View.INVISIBLE);
            }
        });

        Button scanButton = (Button) findViewById(R.id.access_points_scan_button);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.showToastMessage(toast, "Scan started!", Toast.LENGTH_SHORT);
                progressScanningBTPoints.setVisibility(View.VISIBLE);
                locator.lookForAccessPointsForPosition(currentPosition, PositionAccessPointsActivity.this.getApplicationContext());
                onUpdatingPointsStarted();
            }
        });

        saveButton = (Button) findViewById(R.id.access_points_save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //delete AP from another positions as one AP can only be assigned to one position
                for (BluetoothAP ap : currentPosition.getBluetoothPoints()) {
                    Position found = FirebaseWrapper.getInstance().getPositionByAP(ap);
                    if (found != null && !found.equals(currentPosition)) {
                        found.getBluetoothPoints().remove(ap);
                        BluetoothAP foundMainAp = null;
                        for (BluetoothAP foundAp : found.getBluetoothPoints()) {
                            if (foundAp.isMainPoint()) {
                                foundMainAp = foundAp;
                                break;
                            }
                        }
                        if (foundMainAp == null) {
                            ap.setMainPoint(true);
                            found.getBluetoothPoints().add(ap);
                            Utils.showToastMessage(toast,
                                    String.format("Unable to save, because AP %s is already " +
                                                    "assigned to position %s and can not be deleted from there.",
                                            ap.getBssid(), found.getTitle()), Toast.LENGTH_LONG);
                            return;
                        }
                        FirebaseWrapper.getInstance().updateExistingPosition(found);
                    }
                }

                if (currentPosition.getBluetoothPoints().isEmpty()) {
                    Utils.showToastMessage(toast, "No access point is selected!", Toast.LENGTH_SHORT);
                    return;
                } else if (currentPosition.getBluetoothPoints().size() == 1) {
                    currentPosition.getBluetoothPoints().get(0).setMainPoint(true);
                } else {
                    BluetoothAP mainAP = null;
                    for (BluetoothAP ap : currentPosition.getBluetoothPoints()) {
                        if (ap.isMainPoint()) {
                            mainAP = ap;
                            break;
                        }
                    }
                    if (mainAP == null) {
                        // if there is no access point selected as main
                        // we will select first, as it has best signal strength
                        currentPosition.getBluetoothPoints().get(0).setMainPoint(true);
                    }
                }

                if (currentPosition.isStored()) {
                    FirebaseWrapper.getInstance().updateExistingPosition(currentPosition);
                } else {
                    FirebaseWrapper.getInstance().createNewPosition(currentPosition);
                }
                Utils.showToastMessage(toast, "Saved successfully!", Toast.LENGTH_SHORT);
            }
        });

        locator.lookForAccessPointsForPosition(currentPosition, getApplicationContext());
        onUpdatingPointsStarted();
    }

    @Override
    public void addACPoint(BluetoothAP ap) {
        ap.setStored(true);
        if (currentPosition.getBluetoothPoints() == null) {
            List<BluetoothAP> aps = new ArrayList<>();
            aps.add(ap);
            currentPosition.setBluetoothPoints(aps);
        } else if (!currentPosition.getBluetoothPoints().contains(ap)) {
            currentPosition.getBluetoothPoints().add(ap);
        }
    }

    @Override
    public void removeACPoint(BluetoothAP ap) {
        ap.setStored(false);
        if (currentPosition.getBluetoothPoints() != null) {
            currentPosition.getBluetoothPoints().remove(ap);
        }
    }

    @Override
    public void setMainACPoint(BluetoothAP ap) {
        if (currentPosition.getBluetoothPoints() != null) {
            for (BluetoothAP point : currentPosition.getBluetoothPoints()) {
                if (point.getBssid().equals(ap.getBssid())) {
                    point.setMainPoint(true);
                    point.setStored(true);

                    Utils.showToastMessage(toast, "AC point added as main.", Toast.LENGTH_SHORT);
                } else {
                    point.setMainPoint(false);
                }
            }
        }
    }

    private void onUpdatingPointsStarted() {
        recyclerView.setAdapter(new AccessPointsViewAdapter(new ArrayList<>(bluetoothAPs), this, false));
        saveButton.setEnabled(false);
    }

    private void onUpdatingPointsEnded() {
        recyclerView.setAdapter(new AccessPointsViewAdapter(new ArrayList<>(bluetoothAPs), this, true));
        saveButton.setEnabled(true);
    }
}
